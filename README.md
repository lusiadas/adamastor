# Adamastor

> *A plugin repository for [Oh My Fish](https://www.github.com/oh-my-fish/oh-my-fish).*

[![GPL License](https://img.shields.io/badge/license-GPL-blue.svg?longCache=true&style=flat-square)](/LICENSE)
[![Fish Shell Version](https://img.shields.io/badge/fish-v2.7.1-blue.svg?style=flat-square)](https://fishshell.com)
[![Oh My Fish Framework](https://img.shields.io/badge/Oh%20My%20Fish-Framework-blue.svg?style=flat-square)](https://www.github.com/oh-my-fish/oh-my-fish)

<br>

## Description

This repository lists the fish plugins that I've developed for *Oh My Fish*. Some of them are already listed under their [official repository](https://github.com/oh-my-fish/packages-main), others, not yet. Though here, you've access to these plugins as soon as I publish them.

## Adding this repository

`omf repositories add https://gitlab.com/lusiadas/adamastor`

## Contents

### Plugins

| Name | Description |
| :------------- | :------------- |
[contains_opts](https://gitlab.com/lusiadas/contains_opts) | Check if the command line contains any of the listed flags. If no flags are listed, it tests if the command line contains any flag at all.
[insist](https://gitlab.com/lusiadas/insist) | Repeat the previous command line input, either until it is successful, or for a given amount of attempts.
[feedback](https://gitlab.com/lusiadas/nav) | A plugin to display feedback messages neatly. Usually used in other plugins.
[nav](https://gitlab.com/lusiadas/nav) | **Navigational Assistance with Velocity**. In brief, it finds a folder whose name matches search patterns and makes it the current working directory.
[package_list](https://gitlab.com/lusiadas/package_list) | Utility to list manually installed packages upon their installation or removal.
[swap](https://gitlab.com/lusiadas/swap) | A wrapper function to allow mv to swap the location or contents of targeted files or folders.
[pgen](https://gitlab.com/lusiadas/pgen) | **Passphrase Generator**. Generate passphrase with wordlists generated using Wordnik's dictionary, or others.
[publish](https://gitlab.com/lusiadas/publish) | Publish to a pastebin the contents of the clipboard, a file, or several files, either entirelly or partially.
[wal](https://gitlab.com/lusiadas/wal) | List photo collections on unsplash from where to get a wallpaper at random.

### Themes

| Name | Description |
| :------------- | :------------- |
[min](https://gitlab.com/lusiadas/min) | A minimal prompt for the fish shell.
[otacon](https://gitlab.com/lusiadas/otacon) | A theme optimized for the vertical display of mobile devices.
